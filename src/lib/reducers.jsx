/* ************************************* */
/* ********       IMPORTS       ******** */
/* ************************************* */
import { combineReducers } from 'redux'
import beneficiaryModalReducer from '../buisiness/BeneficiarySearchModal/BsmReducer'

/* ************************************* */
/* ********      VARIABLES      ******** */
/* ************************************* */
const reducers = combineReducers({
  bydBusiness: combineReducers({
    beneficiaryModal: beneficiaryModalReducer
  })
})

/* ************************************* */
/* ********  PRIVATE FUNCTIONS  ******** */
/* ************************************* */

/* ************************************* */
/* ********   PUBLIC FUNCTIONS  ******** */
/* ************************************* */

/* ************************************* */
/* ********       EXPORTS       ******** */
/* ************************************* */
export default reducers
