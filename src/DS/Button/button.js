/* ************************************* */
/* ********       IMPORTS       ******** */
/* ************************************* */
import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
// import './css/_button.scss'

/* ************************************* */
/* ********      VARIABLES      ******** */
/* ************************************* */
const mapToCssModules = (className, cssModule) => {
  if (!cssModule) return className
  return className
    .split(' ')
    .map((c) => cssModule[c] || c)
    .join(' ')
}

const propTypes = {
  /** State of button. Active or not. */
  active: PropTypes.bool,
  /** IgnoreDoc */
  block: PropTypes.bool,
  /** Behavior of button. */
  behavior: PropTypes.oneOf([
    'default',
    'primary',
    'secondary',
    'success',
    'info',
    'warning',
    'danger',
    'link'
  ]),
  /** Indicates if the button is a toggle button or not. */
  toggle: PropTypes.bool,
  /** Indicates if the button has a circle shape. */
  circle: PropTypes.bool,
  /** If button is disabled or not. */
  disabled: PropTypes.bool,
  /** If button is outlined or not. */
  outline: PropTypes.bool,
  /** If button is outlined and has no border. */
  outlineNoBorder: PropTypes.bool,
  /** IgnoreDoc */
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  /** IgnoreDoc */
  getRef: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  /** Callback on click event. */
  onClick: PropTypes.func,
  /** Size of button. */
  size: PropTypes.oneOf(['xs', 'sm', 'md', 'xl']),
  /** IgnoreDoc */
  children: PropTypes.node,
  /** Custom class css. */
  className: PropTypes.string,
  /** IgnoreDoc */
  cssModule: PropTypes.object
}

const defaultProps = {
  behavior: 'default',
  tag: 'button',
  toggle: false,
  circle: false
}

/* ************************************* */
/* ********      COMPONENT      ******** */
/* ************************************* */
/** ButtonStorybook component allows to display a Bootstrap button. */
class Button extends React.Component {
  constructor(props) {
    super(props)

    this.onClick = this.onClick.bind(this)
  }

  onClick(e) {
    if (this.props.disabled) {
      e.preventDefault()
      return
    }

    if (this.props.onClick) {
      this.props.onClick(e)
    }
  }

  render() {
    const {
      active,
      block,
      className,
      cssModule,
      behavior,
      outline,
      circle,
      size,
      tag: RawTag,
      getRef,
      toggle,
      outlineNoBorder,
      ...attributes
    } = this.props

    const buttonBehavior =
      outline || outlineNoBorder ? `btn-outline-${behavior}` : `btn-${behavior}`
    const classes = mapToCssModules(
      classNames(
        className,
        'btn',
        buttonBehavior,
        size ? `btn-${size}` : false,
        block ? 'btn-block' : false,
        circle ? 'btn-circle' : false,
        { active, disabled: this.props.disabled },
        outlineNoBorder ? 'btn-outline-no-border' : false
      ),
      cssModule
    )

    let Tag = RawTag
    if (attributes.href && Tag === 'button') {
      Tag = 'a'
    }

    if (toggle) {
      return (
        <Tag
          {...attributes}
          className={classes}
          ref={getRef}
          onClick={this.onClick}
          data-toggle='button'
        />
      )
    }
    return (
      <div>
        anUpdate
        <Tag
          {...attributes}
          className={classes}
          ref={getRef}
          onClick={this.onClick}
        />
      </div>
    )
  }
}

Button.propTypes = propTypes
Button.defaultProps = defaultProps

/* ************************************* */
/* ********       EXPORTS       ******** */
/* ************************************* */
export default Button
