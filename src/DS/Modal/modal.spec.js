describe('modal test', () => {
  it('should access / ', () => {
    cy.visit('http://localhost:6006/iframe.html?path=/story/toto--basic-modal')
    cy.get('.modal-content').should('not.be.visible')
    cy.get('#open-modal').click()
    cy.get('.modal-footer > .btn-primary').should('exist')
    cy.get('.btn-secondary').click()
    cy.get('.modal-content').should('not.be.visible')
  })
})
