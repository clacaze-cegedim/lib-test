import { shallow } from 'enzyme'
import Modal from '../Modal/Modal'
import React from 'react'
import './setup'

describe('modal test', () => {
  const wrapper = shallow(<Modal isOpen={false} />)
  it('should be able to open and close', () => {
    expect(wrapper.props().children.props.style.display).toEqual('none')
    wrapper.setProps({ isOpen: true })
    expect(wrapper.props().children.props.style.display).toEqual('block')
    wrapper.setProps({ isOpen: false })
    expect(wrapper.props().children.props.style.display).toEqual('none')
  })
})
