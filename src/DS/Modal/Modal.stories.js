import React, { useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import { withKnobs, text } from '@storybook/addon-knobs'
import { action } from '@storybook/addon-actions'
import Modal from './Modal'

export default {
  title: 'TOTO',
  component: Modal,
  decorators: [withKnobs]
}

export const BASIC_MODAL = () => {
  const [state, setState] = useState({ isOpen: false })
  return (
    <div>
      <button
        id='open-modal'
        type='button'
        className='btn btn-primary'
        onClick={() => {
          setState({ isOpen: !state.isOpen })
        }}
      >
        Launch demo modal
      </button>
      <Modal
        isOpen={state.isOpen}
        toggle={() => {
          setState({ isOpen: !state.isOpen })
        }}
        onSave={() => {
          setState({ isOpen: !state.isOpen })
        }}
        onClose={() => {
          setState({ isOpen: !state.isOpen })
        }}
        saveText={text('Save texte', 'Save texte')}
        close={text('close', 'close')}
        title={text('title', 'title')}
        body={text('body', 'body')}
      />
    </div>
  )
}

BASIC_MODAL.story = {
  name: 'Basic Modal example'
}
