import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'

const Modal = ({
  isOpen,
  toggle,
  body,
  title,
  onClose,
  close,
  onSave,
  saveText
}) => (
  <div>
    <div
      className='modal'
      tabIndex='-1'
      role='dialog'
      style={isOpen ? { display: 'block' } : { display: 'none' }}
      isOpen={isOpen}
    >
      <div className='modal-dialog' role='document'>
        <div className='modal-content'>
          <div className='modal-header'>
            <h5 className='modal-title'>{title}</h5>
            <button
              type='button'
              className='close'
              onClick={toggle}
              aria-label='Close'
            >
              <span aria-hidden='true'>&times;</span>
            </button>
          </div>
          <div className='modal-body'>
            <p>{body}</p>
          </div>
          <div className='modal-footer'>
            <button type='button' className='btn btn-primary' onClick={onSave}>
              {saveText}
            </button>
            <button
              type='button'
              className='btn btn-secondary'
              onClick={onClose}
            >
              {close}
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default Modal
