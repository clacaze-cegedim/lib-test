/* ************************************* */
/* ********       IMPORTS       ******** */
/* ************************************* */
import uuidV4 from 'uuid/v4';
import isFunction from 'lodash.isfunction';

function mapToCssModules(className, cssModule) {
    if (!cssModule) return className;
    return className
        .split(' ')
        .map(c => cssModule[c] || c)
        .join(' ');
}

function getOriginalBodyPadding() {
    const style = window.getComputedStyle(document.body, null);

    return parseInt((style && style.getPropertyValue('padding-right')) || 0, 10);
}

function setScrollbarWidth(padding) {
    document.body.style.paddingRight = padding > 0 ? `${padding}px` : null;
}

function isBodyOverflowing() {
    return document.body.clientWidth < window.innerWidth;
}

function conditionallyUpdateScrollbar() {
    function getScrollbarWidth() {
        const scrollDiv = document.createElement('div');
        scrollDiv.style.position = 'absolute';
        scrollDiv.style.top = '-9999px';
        scrollDiv.style.width = '50px';
        scrollDiv.style.height = '50px';
        scrollDiv.style.overflow = 'scroll';
        document.body.appendChild(scrollDiv);
        const scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
        document.body.removeChild(scrollDiv);
        return scrollbarWidth;
    }

    const scrollbarWidth = getScrollbarWidth();
    // https://github.com/twbs/bootstrap/blob/v4.0.0-alpha.4/js/src/modal.js#L420
    let fixedContent = 0;
    if (document.querySelectorAll('.navbar-fixed-top, .navbar-fixed-bottom, .is-fixed')) {
        fixedContent = document.querySelectorAll('.navbar-fixed-top, .navbar-fixed-bottom, .is-fixed')[0];
    }

    const bodyPadding = fixedContent ? parseInt(fixedContent.style.paddingRight || 0, 10) : 0;

    if (isBodyOverflowing()) {
        setScrollbarWidth(bodyPadding + scrollbarWidth);
    }
}
function guid() {
    return uuidV4();
}

/**
 * Groups array elements by property value
 *
 * console.log(groupBy(['one', 'two', 'three'], 'length')); // => {3: ["one", "two"], 5: ["three"]}
 *
 * @param array - the array to parse
 * @param property - the property on which the elements are grouped
 */
function groupBy(array, property) {
    return array.reduce((rv, x) => {
        (rv[x[property]] = rv[x[property]] || []).push(x);
        return rv;
    }, {});
}

// These are all setup to match what is in the bootstrap _variables.scss
// https://github.com/twbs/bootstrap/blob/v4-dev/scss/_variables.scss
const TransitionTimeouts = {
    Fade: 150, // $transition-fade
    Collapse: 350, // $transition-collapse
    Modal: 300, // $modal-transition
    Carousel: 600, // $carousel-transition
};

/**
 * Returns a filtered copy of an object with only the specified keys.
 */
function pick(obj, keys) {
    const pickKeys = Array.isArray(keys) ? keys : [keys];
    let { length } = pickKeys;
    let key;
    const result = {};

    while (length > 0) {
        length -= 1;
        key = pickKeys[length];
        result[key] = obj[key];
    }
    return result;
}
/**
 * Returns a new object with the key/value pairs from `obj` that are not in the array `omitKeys`.
 */
function omit(obj, omitKeys) {
    const result = {};
    Object.keys(obj).forEach(key => {
        if (omitKeys.indexOf(key) === -1) {
            result[key] = obj[key];
        }
    });
    return result;
}

function noop() {
    /* no operation */
}

function onEnterEvent(event, fn) {
    if (event.key === 'Enter') {
        fn(event);
    }
}

// Duplicated Transition.propType keys to ensure that Reactstrap builds
// for distribution properly exclude these keys for nested child HTML attributes
// since `react-transition-group` removes propTypes in production builds.
const TransitionPropTypeKeys = [
    'in',
    'mountOnEnter',
    'unmountOnExit',
    'appear',
    'enter',
    'exit',
    'timeout',
    'onEnter',
    'onEntering',
    'onEntered',
    'onExit',
    'onExiting',
    'onExited',
];

const TransitionStatuses = {
    ENTERING: 'entering',
    ENTERED: 'entered',
    EXITING: 'exiting',
    EXITED: 'exited',
};

const keyCodes = {
    esc: 27,
    space: 32,
    tab: 9,
    up: 38,
    down: 40,
};

function DOMElement(props, propName, componentName) {
    if (!(props[propName] instanceof Element)) {
        return new Error(
            `Invalid prop \`${propName}\` supplied to \`${componentName}\`. Expected prop to be an instance of Element. Validation failed.`,
        );
    }
}

function getTarget(target) {
    if (isFunction(target)) {
        return target();
    }

    if (typeof target === 'string' && document) {
        let selection;
        try {
            selection = document.querySelector(target);
        } catch (e) {
            return document.querySelector(`[id='${target}']`);
        }
        if (selection === null) {
            return document.querySelector(`[id='${target}']`);
        }
        return selection;
    }

    return target;
}

/* ************************************* */
/* ********       EXPORTS       ******** */
/* ************************************* */
export {
    mapToCssModules,
    guid,
    getOriginalBodyPadding,
    conditionallyUpdateScrollbar,
    setScrollbarWidth,
    groupBy,
    TransitionTimeouts,
    omit,
    pick,
    TransitionPropTypeKeys,
    TransitionStatuses,
    DOMElement,
    getTarget,
    keyCodes,
    noop,
    onEnterEvent,
};
