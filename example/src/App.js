import React from 'react'

import { ExampleComponent } from 'react-lib-snapshot-testing'
import 'react-lib-snapshot-testing/dist/index.css'

const App = () => {
  return <ExampleComponent text="Create React Library Example 😄" />
}

export default App
