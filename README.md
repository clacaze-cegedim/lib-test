# react-lib-snapshot-testing

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/react-lib-snapshot-testing.svg)](https://www.npmjs.com/package/react-lib-snapshot-testing) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save react-lib-snapshot-testing
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'react-lib-snapshot-testing'
import 'react-lib-snapshot-testing/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [](https://github.com/)
